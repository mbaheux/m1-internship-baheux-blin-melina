"""hh-mitomotif URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import re_path
from . import views


urlpatterns = [
    re_path(r'^$', views.home, name='HHmitosig_home'),
    re_path(r'^about/$', views.view_about, name='about_page'),
    re_path(r'^mTP/$', views.view_mtp, name='mTP_page'),
    re_path(r'^nTP/$', views.view_ntp, name='nTP_page'),
    re_path(r'^waiting/(?P<slug>[\w]{7})/$', views.view_waiting, name='waiting_page'),
    re_path(r'^redirection/$', views.view_redirection, name='redirection_page'),
    re_path(r'^(?P<slug>[\w]{7})/$', views.view_output, name='run_page'),
]
