# Generated by Django 2.1.7 on 2019-04-16 11:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('HHmitosig', '0004_auto_20190415_1635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='run',
            name='input_seq',
            field=models.FileField(max_length=500, upload_to=''),
        ),
    ]
