from django.test import TestCase
import time
from .models import Run, Output
import plotly.offline as py
import plotly.graph_objs as go
import plotly.figure_factory as ff
from django.shortcuts import get_object_or_404
import csv


def read(file, id):
    """
    :param file: input file given with the form
    :return: return the sequence in the terminal
    """
    f = file.open('r')
    li = ''
    for line in f:
        if line.startswith('>'):
            f.readline()
        else:
            li += line
    f.close()
    g = open(f'{id}.fasta', 'a')
    g.write(li)
    return li, len(li)


def wait(id):
    """
    :param id: the current run id
    :return: return nothing, just sleep for 2 mnts
    """
    time.sleep(120)


def plot(slug):
    data = [go.Bar(
        x=[10, 15],
        y=[slug, 'maison'],
        orientation='h'
    )]
    return py.offline.plot(data, filename=slug, auto_open=False)


def csv_reader(filename):
    with open(filename) as csvfile:
        reader=csv.DictReader(csvfile)
        for row in reader:
            p = Output(e_value=row['e_value'])
            p.save()
