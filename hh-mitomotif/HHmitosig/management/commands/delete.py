from django.core.management.base import BaseCommand
from HHmitosig.models import Run
import os

class Command(BaseCommand):
    """
    DELETING FUNCTION
    """
    def handle(self, *args, **options):
        print('Deleting all Run records!')
        Run.objects.all().delete()
        os.system()
        print('Successfully deleted all Run records!')
