from django import forms
from .models import Run


# class InputForm(forms.Form):
   # sequence = forms.CharField(widget=forms.Textarea,
                              # help_text='Paste ONE input sequence in FASTA format here',
                              # required=True)


class mTPForm(forms.ModelForm):
    """
    Form to mTP run
    :model: Our model Run
    :fields: input_seq and email
    """
    class Meta:
        model = Run
        fields = ['input_seq', 'email']
        labels = {
            'input_seq': 'Enter one FASTA sequence',
        }

    def clean_input_seq(self):
        input_seq = self.cleaned_data['input_seq']
        return input_seq


class nTPForm(forms.ModelForm):
    """
    Form to mTP run
    :model: Our model Run
    :fields: input_seq and email
    """
    class Meta:

        model = Run
        fields = ['input_seq', 'email']
        labels = {
            'input_seq': 'Enter one FASTA sequence',
        }

    def clean_input_seq(self):
        input_seq = self.cleaned_data['input_seq']
        return input_seq
