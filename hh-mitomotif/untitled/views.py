from django.http import HttpResponse
from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import redirect, render
from .forms import ContactForm


def main_home(request):
	"""
	:return: rendering the home page
	"""
	return render(request, 'home.html')


def redirect_home(request):
	"""
	:return: redirection to home page
	"""
	return redirect('/home/')


def contact(request):
	"""
	:return: sending the message and redirecting to success contact page
	:return: rendering the contact page
	"""
	if request.method == 'GET':
		form = ContactForm()
	else:
		form = ContactForm(request.POST)
		if form.is_valid():
			Subject = form.cleaned_data['Subject']
			Message = form.cleaned_data['Message']
			Sender = form.cleaned_data['Sender']
			try:
				send_mail(Subject, Message, Sender, ['admin@admin.com'])
			except BadHeaderError:
				return HttpResponse('Invalid header found.')
			return redirect('/contactSuccess')
	return render(request, "contact.html", {'form': form})


def successView(request):
	"""
	:return: rendering the success contact page
	"""
	return render(request, 'success.html')


def about(request):
	"""
	:return: rendering the about page
	"""
	return render(request, 'about.html')
