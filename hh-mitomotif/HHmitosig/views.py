from django.http import HttpResponse
from django.shortcuts import redirect, render, get_object_or_404
from django.utils import timezone
from django.urls import reverse
from .forms import mTPForm, nTPForm
from .models import Run, is_fasta
from .tests import read, plot, csv_reader
from background_task import background


def view_redirection(request):
    """
    :return: Rendering the redirection page
    """
    return HttpResponse("You will be redirected soon")


def home(request):
    """
    :return: rendering the application's home page
    """
    return render(request, 'home_mitosig.html')


def view_mtp(request):
    """
    :return: rendering the mTP page
    mTP's input form
    """
    if request.method == "POST":
        form = mTPForm(request.POST or None, request.FILES)
        if form.is_valid():
            run_instance = form.save()
            run_instance.input_date = timezone.now().replace(microsecond=0, second=0)
            run_instance.output_date = timezone.now().replace(microsecond=0, second=0)
            run_instance.type = True
            run_instance.output = read(run_instance.input_seq, run_instance.id)
            url = reverse("HHmitosig:waiting_page", kwargs={"slug": run_instance.id})
            run_instance.save()
            return redirect(url, slug=run_instance)
    else:
        form = mTPForm()
    return render(request, 'mTP.html', {'form': form})


def view_ntp(request):
    """
    :return: rendering the nTP page
    nTP's input form
    """
    if request.method == "POST":
        form = nTPForm(request.POST or None, request.FILES)
        if form.is_valid():
            run_instance = form.save()
            run_instance.type = False
            run_instance.input_date = timezone.now().replace(microsecond=0, second=0)
            run_instance.output_date = timezone.now().replace(microsecond=0, second=0)
            run_instance.output = csv_reader('./spP52893ALAMYEASTNP_013190.csv')
            run_instance.save()
            url = reverse("HHmitosig:waiting_page", kwargs={"slug": run_instance.id})
            return redirect(url, slug=run_instance)
    else:
        form = mTPForm()
    return render(request, 'nTP.html', {'form': form})


def view_output(request, slug):
    """
    :param slug: current run id
    :return: rendering the output page for mTP and nTP
    """
    run = get_object_or_404(Run, id=slug)
    run.output = plot(slug)
    if run.input_date == run.output_date:
        run.output_date = timezone.now().replace(microsecond=0)
        run.save()
    return render(request, 'output.html', {'run': run})


def view_about(request):
    """
    :return: rendering the application's about page
    """
    return HttpResponse(f'About HH-MitoSig')


def view_waiting(request, slug):
    """
    :param slug: current run id
    :return: rendering the waiting page for nTP and mTP
    """
    return render(request, 'waiting.html')


