from django import forms


class ContactForm(forms.Form):
	"""
	Contact form, three fields, the subject, the message and the sender's e-mail
	"""
	Subject = forms.CharField(max_length=100, required=True)
	Message = forms.CharField(widget=forms.Textarea, required=True)
	Sender = forms.EmailField(label='e-mail address', required=True)
