import django_tables2 as tables
from .models import *


class OutputTable(tables.Table):
    class Meta:
        model = Output
        fields = ('full_query', 'match_columns', 'neff', 'hit', 'probability', 'e_value', 'score',
                  'aligned_cols', 'identities', 'similarity', 'sum_probs', 'template_neff', 'q0_start', 'q0_align',
                  'q0_end', 'q0_total', 'qc_start', 'qc_align', 'qc_end', 'qc_total', 'alignment',
                  'tc_start', 'tc_align', 'tc_end', 'tc_total', 'confidence', 'type_seq')

