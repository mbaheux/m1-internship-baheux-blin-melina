from django.contrib import admin
from .models import Run
# Register your models here.


class RunAdmin(admin.ModelAdmin):
    """
    Content we see in admin page
    """
    list_display = ('type', 'id', 'email', 'input_date', 'output_date')
    list_filter = ('type', 'email', 'output_date')
    date_hierarchy = 'input_date'
    ordering = ('input_date', )
    search_fields = ('email', 'id')


admin.site.register(Run, RunAdmin)
