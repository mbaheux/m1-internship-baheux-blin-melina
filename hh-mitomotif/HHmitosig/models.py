import uuid
from django.db import models
from django.utils import timezone
from django.utils.http import int_to_base36
from django.core.exceptions import ValidationError
from Bio import SeqIO

ID_length = 7                                                           # Length of generated IDs


def id_gen() -> str:
    """
    generate a ID number to be used as Primary key
    """
    return int_to_base36(uuid.uuid4().int)[:ID_length]


def is_fasta(filename):
    """
    :param filename: our input file
    just verify if the file is a fasta input
    """
    n = 0
    print(filename)
    test = filename.open('r')
    for record in SeqIO.parse(test, 'fasta'):
        n += 1
    if n == 0:
        raise ValidationError("No FASTA sequence detected")
    elif n == 1:
        return filename
    else:
        raise ValidationError("There should be only one FASTA sequence per file")


def parseFasta(filename):
    """
    :param filename: name of the input file
    :return: name of the input file or an error message
    """
    f = filename.open('r')
    line = f.readline()
    n = 0
    seq=[]
    while True:
        if line.startswith(b'>'):
            n += 1
            line = f.readline()
        else:
            decoded = line.decode()
            seq.append(decoded)
            if '\n' in seq:
                seq.remove('\n')
            line = f.readline()
        if line == b'':
            break
    seq2 = ''.join(seq)
    if n == 1:
        if len(seq2) > 100:
            k = 0
            p = 0
            for i in seq2:
                if i in ['G', 'A', 'V', 'L', 'I', 'S', 'T', 'C', 'M', 'P', 'F', 'Y', 'W', 'D', 'E', 'N', 'Q', 'K', 'R', 'H', 'U', 'X', '\n']:
                    k += 1
                else:
                    p += 1
            if len(seq2) == k:
                return filename
            else:
                raise ValidationError("Invalid Fasta sequence")
        else:
            raise ValidationError("Fasta sequence too short")
    elif n == 0:
        raise ValidationError("No FASTA sequence detected")
    else:
        raise ValidationError("There should be only one FASTA sequence per file")


# Create your models here.
class Run(models.Model):
    """
    :id: primary key used to identify a run. Auto input
    :email: a charfield  filled with an default mail
    :input_seq: a file field to input the sequence
    :type: boolean variable, True if mTP, False if nTP
    :input_date: start of the run date
    :output: output file
    :output_date: end of the run date
    """
    id = models.CharField(max_length=ID_length,
                          primary_key=True,
                          default=id_gen,
                          editable=False)
    email = models.CharField(max_length=20,
                             default='youpi@gmail.com',
                             null=True)
    input_seq = models.FileField(validators=[parseFasta])
    input_date = models.DateTimeField(default=timezone.now,
                                      verbose_name="input date")
    type = models.BooleanField(default=True)  # True = mTP, False = nTP
    output = models.CharField(max_length=50,
                              null=True)
    output_date = models.DateTimeField(default=timezone.now,
                                       verbose_name="output date")

    class Meta:
        verbose_name = "run_instance"
        ordering = ['input_date']

    def __str__(self):
        return self.id

    def set_email(self, new_email):
        Run.email = new_email

    def set_type(self, new_type):
        Run.type = new_type

    def start_countdown(self, new_output_date):
        output_date = new_output_date


class Output(models.Model):
    full_query = models.CharField(verbose_name='full_query'),
    match_columns = models.IntegerField(),
    neff = models.FloatField(),
    no = models.IntegerField(),
    hit = models.CharField(),
    probability = models.FloatField(),
    e_value = models.FloatField(max_length=100),
    score = models.FloatField(),
    aligned_cols = models.IntegerField(),
    identities = models.CharField(max_length=4),
    similarity = models.FloatField(),
    sum_probs = models.FloatField(),
    template_neff = models.FloatField(),
    q0_start = models.IntegerField(),
    q0_align = models.CharField(),
    q0_end = models.IntegerField(),
    q0_total = models.IntegerField(),
    qc_start = models.IntegerField(),
    qc_align = models.CharField(),
    qc_end = models.IntegerField(),
    qc_total = models.IntegerField(),
    alignment = models.CharField(),
    tc_start = models.IntegerField(),
    tc_align = models.CharField(),
    tc_end = models.IntegerField(),
    tc_total = models.IntegerField(),
    confidence = models.CharField(),
    type_seq = models.CharField(max_length=50)


